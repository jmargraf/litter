from dash import Dash, dash_table, html, callback, Input, Output, State, dcc
import dash_bootstrap_components as dbc
import pandas as pd
from litter import update_df, formatted_citation
import sys

class LiteratureData:
    def __init__(self,bibname):
        self.bibname = bibname
        #print(self.bibname)
        try:
            self.df = pd.read_hdf(self.bibname)
            self.df = self.df.sort_values(by=["year"], ascending=False)
        except:
            self.df = pd.DataFrame()
    def update(self,df):
        self.df = df

if len(sys.argv)==1:
    print("provide path to bibliography")
    print(sys.argv)
    exit()
else:
    path = sys.argv[1]
    #df = pd.DataFrame()
    lit = LiteratureData(path)

app = Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP],
        requests_pathname_prefix='/litter/',
        routes_pathname_prefix='/litter/')

app.title = "Litter v0.1"

app.layout = dbc.Container(
    [
        dbc.Row(
            [
                dbc.Col(
                    dash_table.DataTable(
                        lit.df.to_dict("records"),
                        [
                            {"name": i, "id": i}
                            for i in ["year", "shortJournal", "title", "authors", "key"]
                        ],
                        id="lit_datatable",
                        style_data={
                            "whiteSpace": "normal",
                            "height": "auto",
                        },
                    ),
                    id="left",
                    width=8,
                    className="h-100 overflow-scroll",
                ),
                dbc.Col(
                    [
                        html.H1("\U0001F9D0 Litter v0.1"),
                        html.Br(),
                        dcc.Input(id="doiinp", value="Enter DOI", type="text"),
                        dbc.Button(id="submit-doi", n_clicks=0, children="Fetch!"),
                        html.Br(),
                        html.Br(),
                        html.Br(),
                        html.H4("Bibtex"),
                        dcc.Clipboard(
                            target_id="tbl_out",
                            title="copy_bib",
                            style={
                                "display": "inline-block",
                                "fontSize": 20,
                                "verticalAlign": "top",
                            },
                        ),
                        dbc.Alert(id="tbl_out"),
                        html.Br(),
                        html.H4("Formatted Citation"),
                        dcc.Clipboard(
                            target_id="form_out",
                            title="copy_form",
                            style={
                                "display": "inline-block",
                                "fontSize": 20,
                                "verticalAlign": "top",
                            },
                        ),
                        dbc.Alert(id="form_out"),
                    ],
                    id="right",
                    width=4,
                ),
            ],
            className="vh-100",
        ),
    ],
    id="content",
)


@callback(
    Output("tbl_out", "children"),
    Output("form_out", "children"),
    Input("lit_datatable", "active_cell"),
)
def update_bibtex_display(active_cell):
    if active_cell:
        ind = active_cell["row"]
        bib = lit.df.iloc[[ind]]["bibtexentry"]
        # try:
        for index, row in lit.df.iloc[[ind]].iterrows():
            form = formatted_citation(row)
        # except:
        #    form = 'citation error'
    return (bib, form) if active_cell else ("Click the table", " ")


@callback(
    Output("lit_datatable", "data"),
    Output("lit_datatable", "active_cell"),
    Input("submit-doi", "n_clicks"),
    State("doiinp", "value"),
)
def update_table(n_clicks, input1):
    #global df
    table = lit.df.to_dict("records")
    active_cell = {'row': 0, 'column': 0, 'column_id': 'year'}
    if input1 != "Enter DOI":
        df, ref = update_df(input1, lit.bibname)
        df = df.sort_values(by=["year"], ascending=False)
        table = df.to_dict("records")
        lit.update(df)
        for irow,row in enumerate(table):
            if ref['key'] in row.values():
                active_cell = {'row': irow, 'column': 4, 'column_id': 'key'}
        #active_cell = {'row': 0, 'column': 0, 'column_id': 'year'}
        #print(input1)
    return table, active_cell


if __name__ == "__main__":
    app.run_server(debug=True, host='0.0.0.0', port=8050)
