import urllib, urllib.request
import xml.dom.minidom
import re
import unicodedata


def getArxiv(arxivid, doiKey):
    url = "http://export.arxiv.org/api/query?id_list=" + arxivid
    data = urllib.request.urlopen(url)
    doc = data.read()
    # Parse it
    doc = xml.dom.minidom.parseString(doc)

    res = {}

    authors = doc.getElementsByTagName("author")
    res["authors"] = []
    for author in authors:
        name = author.getElementsByTagName("name")[0]
        namedat = name.firstChild.data.split()
        surname = namedat[-1]
        givenName = ""
        for name in namedat[:-1]:
            givenName += name + " "
        authorstring = f"{surname}, {givenName}"
        res["authors"].append(authorstring[:-1])
        # print(f"{surname}, {givenName}")
        # print(name.firstChild.data)

    titles = doc.getElementsByTagName("title")
    for title in titles[1:]:
        # print(title.firstChild.data)
        titlestring = (
            title.firstChild.data.replace("\n", "").replace("\r", "").replace("  ", " ")
        )
        res["title"] = titlestring

    ids = doc.getElementsByTagName("id")
    for id in ids[1:]:
        # print(id.firstChild.data)
        doi = id.firstChild.data[21:]
        res["doi"] = doi

    years = doc.getElementsByTagName("published")
    for year in years:
        # print(year.firstChild.data[:4])
        yr = year.firstChild.data[:4]
        res["year"] = yr

    # Create a citation key
    if doiKey:
        key = doi
    else:
        r = re.compile("\W")
        if len(res["authors"]) > 0:
            key = r.sub(
                "", res["authors"][0].split(",")[0]
            )  # .encode('ascii', 'ignore')
            key = unicodedata.normalize("NFKD", key).encode("ascii", "ignore").decode()
            #      print(key)
            # key.encode('ascii', 'ignore')
        else:
            key = ""
        if res["year"] is not None:
            key = key + res["year"]
        if res["title"] is not None:
            dat = res["title"].split()
            for word in dat:
                if len(word) > 3:
                    key = key + word
                    break

    res["key"] = key

    res["fullJournal"] = None
    res["shortJournal"] = "arXiv:" + res["doi"]
    res["volume"] = None
    res["issue"] = None
    res["firstPage"] = None
    res["lastPage"] = None

    # print(res)
    return res


if __name__ == "__main__":
    getArxiv("2011.14115", False)
    getArxiv("2011.14115v3", False)
