import pandas as pd
from doi2bib import getCrossRef, bibtexEntry
from arxiv2bib import getArxiv
from chemrxiv2bib import getChemrxiv
import sys

import warnings

warnings.filterwarnings("ignore", category=pd.io.pytables.PerformanceWarning)

def authorlist_to_string(alist):
    string = ""
    for i, author in enumerate(alist):
        string += author
        if i < len(alist) - 1:
            string += " and "
    return string


def update_df(doi,path):
    main_df = pd.read_hdf(path)
    nextindex = len(main_df)
    doiKey = False
    try:
        ref = getCrossRef(doi, doiKey)
    except:
        try:
            print("doi not found, trying arXiv")
            ref = getArxiv(doi, doiKey)
        except:
            print("arXiv not found, trying ChemRxiv")
            ref = getChemrxiv(doi, doiKey)

    bib = bibtexEntry(ref)

    ref["authors"] = authorlist_to_string(ref["authors"])
    ref["bibtexentry"] = bib

    temp_df = pd.DataFrame(ref, index=[nextindex])

    if doi in main_df.values:
        print("doi already included")
    else:
        main_df = pd.concat([main_df,temp_df])
        main_df.to_hdf(path, "all")
    return main_df, ref


def formatted_citation(row):
    citation = ""
    # print(row)
    if row["authors"] is not None:
        authors = row["authors"].split(" and ")
        for author in authors:
            surname, givennames = author.split(",")
            initials = ""
            for name in givennames.split():
                initials += name[0] + "."
            citation += initials + " " + surname + ", "
        citation = citation[:-2]
        citation += " "
    if row["year"] is not None:
        citation += f" ({row['year']}) "
    if row["shortJournal"] is not None:
        citation += row["shortJournal"] + ", "
    if row["volume"] is not None:
        citation += row["volume"] + ", "
    if row["firstPage"] is not None:
        citation += row["firstPage"] + "."

    if citation[-2:] == ", ":
        citation = citation[:-2] + "."

    return citation


if __name__ == "__main__":

    # Clean bib file with most recent version
    # main_df = pd.DataFrame()

    old_df = pd.read_hdf("bib_copy.hdf")

    # print(main_df)
    # print(len(main_df))

    main_df = pd.DataFrame()

    for index, row in old_df.iterrows():
        # main_df = pd.DataFrame()
        doi = row["doi"]
        print(formatted_citation(row))
        nextindex = len(main_df)

        # if len(sys.argv) > 1:
        #    doi = sys.argv[1]
        doiKey = False
        try:
            ref = getCrossRef(doi, doiKey)
        except:
            try:
                print("doi not found, trying arXiv")
                ref = getArxiv(doi, doiKey)
            except:
                print("arXiv not found, trying ChemRxiv")
                ref = getChemrxiv(doi, doiKey)

#        try:
#            ref = getCrossRef(doi, doiKey)
#        except:
#            print("doi not found, trying arXiv")
#            ref = getArxiv(doi, doiKey)

        bib = bibtexEntry(ref)

        ref["authors"] = authorlist_to_string(ref["authors"])
        ref["bibtexentry"] = bib

        temp_df = pd.DataFrame(ref, index=[nextindex])

        if doi in main_df.values:
            print("doi already included")
        else:
            #main_df = main_df.append(temp_df)
            main_df = pd.concat([main_df,temp_df])
            main_df.to_hdf("bib.hdf", "all")

        # main_df.replace(to_replace=[None], value=pd.NA, inplace=True)
    print(main_df)
