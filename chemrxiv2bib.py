import urllib, urllib.request
import re
import unicodedata
import json

def getChemrxiv(doi, doiKey):
    url = "https://chemrxiv.org/engage/chemrxiv/public-api/v1/items/doi/" + doi
    print(url)
    req  = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    data = urllib.request.urlopen(req)

    # Parse it
    data = json.load(data)
    res = {}

    authors = data["authors"]
    res["authors"] = []
    for author in authors:
        surname = author["lastName"]
        givenName = author["firstName"]
        authorstring = f"{surname}, {givenName}"
        res["authors"].append(authorstring)

    res["title"] = data["title"]
    res["doi"] = data["doi"] 
    res["year"] = data["statusDate"][:4] 

    # Create a citation key
    if doiKey:
        key = doi
    else:
        r = re.compile("\W")
        if len(res["authors"]) > 0:
            key = r.sub(
                "", res["authors"][0].split(",")[0]
            )  # .encode('ascii', 'ignore')
            key = unicodedata.normalize("NFKD", key).encode("ascii", "ignore").decode()
            #      print(key)
            # key.encode('ascii', 'ignore')
        else:
            key = ""
        if res["year"] is not None:
            key = key + res["year"]
        if res["title"] is not None:
            dat = res["title"].split()
            for word in dat:
                if len(word) > 3:
                    key = key + word
                    break

    res["key"] = key

    res["fullJournal"] = None
    res["shortJournal"] = "ChemRxiv:" + res["doi"]
    res["volume"] = None
    res["issue"] = None
    res["firstPage"] = None
    res["lastPage"] = None

    print(res)
    return res

if __name__ == "__main__":
    getChemrxiv("10.26434/chemrxiv-2022-66xhb", False)
    #getArxiv("2011.14115v3", False)
